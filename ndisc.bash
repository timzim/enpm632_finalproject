#!/bin/bash
# Sends random neighbor solicitations for testing the DoS program
# Requires the ndisc6 package
for i in {1..25}
do
  # Generate 16-bits for the lower address
  N=$(printf "%04x" $RANDOM)
  # Call ndisc to send a solicitation from the user space
  ndisc6 fe80::1d0:a75d:753b:$N enp0s3
  # Wait 100 milliseconds until the next solicitation
  sleep 0.1
done
